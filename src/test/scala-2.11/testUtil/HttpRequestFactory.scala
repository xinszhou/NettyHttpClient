package testUtil

import java.nio.charset.StandardCharsets
import java.util.UUID

import io.netty.buffer.{ByteBuf, Unpooled}
import io.netty.handler.codec.http._
import util.GlobalConfig

/**
  * Created by xinszhou on 16/6/14.
  */
object HttpRequestFactory {

  def getLocalESMeta(): FullHttpRequest = {
    val request: FullHttpRequest = new DefaultFullHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, "/")

    request.headers.set(HttpHeaderNames.HOST, GlobalConfig.esServer.host)
    request.headers.set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE)
    request.headers.add(HttpHeaderNames.CONTENT_TYPE, "application/json")

    return request
  }

  def sendRandomInt(): FullHttpRequest = {
    val uuid = UUID.randomUUID()

    val request: FullHttpRequest = new DefaultFullHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.POST, "/")

    request.headers.set(HttpHeaderNames.HOST, "localhost:19201")
    request.headers.set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE)

    val buf: ByteBuf = Unpooled.copiedBuffer(uuid.toString, StandardCharsets.UTF_8)
    request.headers().set(HttpHeaderNames.CONTENT_LENGTH, buf.readableBytes());
    request.content().clear().writeBytes(buf)
    return request
  }

}
