package server.httpServer

import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.{ChannelInitializer, ChannelOption}
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.SocketChannel
import io.netty.channel.socket.nio.NioServerSocketChannel
import io.netty.handler.codec.http.{HttpRequestDecoder, HttpRequestEncoder, HttpResponseEncoder}

/**
  * Created by xinszhou on 16/8/15.
  */
class NettyHttpServer(port: Int) {

  def run(): Unit = {
    val bossGroup = new NioEventLoopGroup()
    val workerGroup = new NioEventLoopGroup()

    try {
      val serverBootstrap = new ServerBootstrap()
      serverBootstrap.group(bossGroup, workerGroup)
        .channel(classOf[NioServerSocketChannel])
        .childHandler(new ChannelInitializer[SocketChannel] {
          override def initChannel(ch: SocketChannel): Unit =
            ch.pipeline()
              .addLast(new HttpRequestDecoder())
              .addLast(new HttpResponseEncoder)
              .addLast(new ServerConnectionHandler)

        })
        .option[Integer](ChannelOption.SO_BACKLOG, 128)
        .childOption[java.lang.Boolean](ChannelOption.SO_KEEPALIVE, true)

      val channelFuture = serverBootstrap.bind(port).sync()
      channelFuture.channel().closeFuture().sync()
    } finally {
      bossGroup.shutdownGracefully()
      workerGroup.shutdownGracefully()
    }
  }
}

object NettyHttpServer extends App {

  val port = 19292
  new NettyHttpServer(port).run

}