package server.httpServer

import io.netty.buffer.Unpooled
import io.netty.channel.{ChannelFutureListener, ChannelHandlerContext, SimpleChannelInboundHandler}
import io.netty.handler.codec.http._
import io.netty.util.CharsetUtil

/**
  * Created by xinszhou on 16/8/15.
  */
class ServerConnectionHandler extends SimpleChannelInboundHandler[Object] {
  var httpRequest: HttpRequest = _


  override def channelRead0(ctx: ChannelHandlerContext, msg: Object): Unit = {
    val strBuf = new StringBuilder
    println("channelread0 called")

    println(msg.getClass)

    if (msg.isInstanceOf[HttpRequest]) {
      httpRequest = msg.asInstanceOf[HttpRequest]
    } else if (msg.isInstanceOf[HttpContent]) {
      val content = msg.asInstanceOf[HttpContent]
      strBuf.append(content.content().toString(CharsetUtil.UTF_8))

      if (msg.isInstanceOf[LastHttpContent]) {

        val response = new DefaultFullHttpResponse(
          HttpVersion.HTTP_1_1, HttpResponseStatus.OK,
          Unpooled.copiedBuffer(strBuf.toString(), CharsetUtil.UTF_8)
        )
        response.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/plain; charset=UTF-8")

//        val isKeepAlive = HttpUtil.isKeepAlive(httpRequest)

        //        if(isKeepAlive) {
//        response.headers().set(HttpHeaderNames.CONNECTION, HttpHeaderNames.KEEP_ALIVE)
        response.headers().setInt(HttpHeaderNames.CONTENT_LENGTH, response.content().readableBytes())
        //        }

        ctx.writeAndFlush(response)
//        ctx.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);

      }
    }

  }
}
