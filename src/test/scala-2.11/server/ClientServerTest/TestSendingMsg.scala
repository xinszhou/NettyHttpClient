package server.ClientServerTest

import java.nio.charset.StandardCharsets
import java.util.UUID
import java.util.concurrent.{ConcurrentHashMap, ConcurrentLinkedQueue}

import httpclient.HttpConnection
import io.netty.buffer.{ByteBuf, Unpooled}
import io.netty.handler.codec.http._
import io.netty.util.CharsetUtil
import org.junit.Before
import org.scalatest.FunSuite
import org.slf4j.LoggerFactory
import pool.{ConnectionPoolThreadSafe, HttpConnectionFactory}
import testUtil.HttpRequestFactory
import util.{FutureUtils, GlobalConfig}

import scala.collection.mutable.ListBuffer
import scala.concurrent.Await
import scala.concurrent.duration._
/**
  * Created by xinszhou on 16/8/15.
  */
class TestSendingMsg extends FunSuite {

  var logger = LoggerFactory.getLogger(getClass)

  val factory = new HttpConnectionFactory(GlobalConfig.esServer)
  val connectionPool = new ConnectionPoolThreadSafe[HttpConnection](factory)

  def createMsg(uuid: String): HttpRequest = {

    val request: FullHttpRequest = new DefaultFullHttpRequest(HttpVersion.HTTP_1_1,
      HttpMethod.POST, "/")

    request.headers.set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE)

    val buf: ByteBuf = Unpooled.copiedBuffer(uuid.toString, StandardCharsets.UTF_8)
    request.headers().set(HttpHeaderNames.CONTENT_LENGTH, buf.readableBytes())
    request.content().clear().writeBytes(buf)
    request
  }

  test("send one message") {
    val uuid = UUID.randomUUID()
    val request = createMsg(uuid.toString)
    val response = Await.result(connectionPool.sendQuery(request), 5 second)
    val body = response.asInstanceOf[FullHttpResponse].content().toString(CharsetUtil.UTF_8)

    assert(uuid.toString === body)

  }

  /**
    * i should be smaller than 1k and bigger than 10
    * because ConnectionPool current level is 10 and waiting queue size is 1000
    */
  test("send 1k message") {
    val threadList = new ListBuffer[Thread]

    var i = 0

    while (i < 1000) {
      val thread: Thread = new Thread(new Runnable {
        override def run(): Unit = {
          val uuid = UUID.randomUUID().toString
          val responseEntity = connectionPool.sendQuery(createMsg(uuid))
          val result = FutureUtils.awaitFuture(responseEntity).asInstanceOf[FullHttpResponse]
          assert(result.content().toString(CharsetUtil.UTF_8) === uuid)
        }
      })

      threadList += thread
      i += 1
    }

    threadList.toList.foreach(_.start())
    threadList.toList.foreach(_.join())
  }

  //response order no necessary same with request order
  test("make sure our netty server support concurrent request") {
    val threadList = new ListBuffer[Thread]
  import scala.concurrent.ExecutionContext.Implicits.global
    var i = 0

    val record = new ConcurrentLinkedQueue[Int]()

    while(i < 100) {

      val thread: Thread = new Thread(new Runnable {
        val j = i
        override def run(): Unit = {
          //cannot do this, big mistake
          //          val j = i, j will always be 100
          val request = createMsg(j + "")
          val responseEntity = connectionPool.sendQuery(createMsg(j + ""))

          responseEntity.foreach(x => {
            val y = x.asInstanceOf[FullHttpResponse].content().toString(CharsetUtil.UTF_8)
            println(y)
            assert(y == j + "")
          })
        }
      })

      threadList += thread
      i += 1
    }

    threadList.toList.foreach(_.start())
    threadList.toList.foreach(_.join())

    Thread.sleep(2000)
  }


}
