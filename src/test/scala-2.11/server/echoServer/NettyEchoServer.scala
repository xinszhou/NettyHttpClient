package server.echoServer

import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.SocketChannel
import io.netty.channel.socket.nio.NioServerSocketChannel
import io.netty.channel.{ChannelInitializer, ChannelOption}
import server.httpServer.ServerConnectionHandler

/**
  * Created by xinszhou on 16/8/15.
  */
class NettyEchoServer(port: Int) {

  def run(): Unit = {
    val bossGroup = new NioEventLoopGroup()
    val workerGroup = new NioEventLoopGroup()

    try {
      val serverBootstrap = new ServerBootstrap()

      serverBootstrap.group(bossGroup, workerGroup)
        .channel(classOf[NioServerSocketChannel])
        .childHandler(new ChannelInitializer[SocketChannel] {
          override def initChannel(ch: SocketChannel): Unit = ch.pipeline().addLast(new ServerConnectionHandler)
        })
        // The maximum queue length for incoming connection indications (a request to connect) is set
        // to the backlog parameter. If a connection indication arrives when the queue is full,
        // the connection is refused.
        .option[java.lang.Integer](ChannelOption.SO_BACKLOG, 128)
        .childOption[java.lang.Boolean](ChannelOption.SO_KEEPALIVE, true)

      val channelFuture = serverBootstrap.bind(port).sync()
      channelFuture.channel().closeFuture().sync()

    } finally {
      workerGroup.shutdownGracefully()
      bossGroup.shutdownGracefully()
    }

  }

}

object NettyEchoServer extends App {
  val port = 19293
  new NettyEchoServer(port).run
}